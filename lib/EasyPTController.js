/**
 * @author Abhishek Shrivastava <shrivasa[at]deshaw.com>
 * @description EasyPT Controller class that coordinates all the work across components
 */

var EasyPTController = function(activeUser, activeWeekRange) {
	this.activeUser = activeUser;
	this.activeWeekRange = activeWeekRange;
	this.DesflowBridge = new _DesflowBridge(activeUser, activeWeekRange);
	this.UserStorage = new _UserStorage(activeUser, activeWeekRange);	
};

EasyPTController.prototype.init = function(renderCallback) {
	
	this.workingDays = this.DesflowBridge.weekWorkingDays();
	
	this.allRequestNumbers = new Array();	
	this.mgmtRequests = new Array();
	var len = MANAGEMENT_REQUESTS.length;
	for(var i=0; i<len; i++) {
		this.mgmtRequests.push(new DesflowRequest(DEFAULT_BA, MANAGEMENT_REQUESTS[i], this.activeUser));
		this.allRequestNumbers.push(Number(MANAGEMENT_REQUESTS[i]));
	}	
	
	this.weeklyRequests = this.DesflowBridge.fetchWeeklyRequests();
	// Ensure uniqueness w.r.t. management requests
	for(var i=0; i<this.weeklyRequests.length; i++) {
		if(this.allRequestNumbers.indexOf(this.weeklyRequests[i].number) != -1) {
			this.weeklyRequests.splice(i, 1);
			--i;
		}
		else {
			this.allRequestNumbers.push(this.weeklyRequests[i].number);
		}
	}
	
	// fetchAdhocRequests is an Async API. This should be at the LAST for renderCallback to work as expected.
	this.adhocRequests = false;
	var thisobj = this;
	this.UserStorage.fetchAdhocRequests(function(requests) {
		thisobj.adhocRequests = new Array();
		var list = requests || new Array();	
		for(var i=0; i<list.length; i++) {
			if(thisobj.allRequestNumbers.indexOf(Number(list[i])) == -1) {
				thisobj.adhocRequests.push(new DesflowRequest(DEFAULT_BA, list[i], this.activeUser, true));
				thisobj.allRequestNumbers.push(Number(list[i]));
			}			
		}
		
		if(renderCallback) {
			renderCallback();
		}
	}, function() {		
		console.log("Unable to load adhoc requests list");
		if(renderCallback) {
			renderCallback();
		}		
	});		
};

EasyPTController.prototype.getRequestDataFromDesflow = function(request) {
	return this.DesflowBridge.readRequest(request);
};

EasyPTController.prototype.getToBeUpdatedList = function() {
	var allRequests = [this.mgmtRequests, this.weeklyRequests, this.adhocRequests];
	var updateList = new Array();
	for(var k=0; k<allRequests.length; k++) {
		for(var i=0; i<allRequests[k].length; i++) {
			if(allRequests[k][i].newpt.length > 0) {
				updateList.push(allRequests[k][i]);
			}
		}
	}
	this.updateList = updateList;
	return updateList;
}

EasyPTController.prototype.saveRequest = function(request, pt_list) {
	this.UserStorage.saveRequestPT(request, pt_list);	
};

EasyPTController.prototype.loadRequest = function(request, callback, error) {
	this.UserStorage.loadRequestPT(request, callback, error);
};

EasyPTController.prototype.clearRequestPT = function(request) {
	this.UserStorage.clear(String(request));
};

EasyPTController.prototype.desflowPush = function(request, ts, details) {
	this.DesflowBridge.updatePT(request, ts, details);
};

EasyPTController.prototype.pushAllToDesflow = function() {
	
	// Using the updateList variable just to be sure that we're pushing what we displayed to the user in confirm box	
	if(this.updateList && this.updateList.length > 0) {
		var allRequests = [this.mgmtRequests, this.weeklyRequests, this.adhocRequests];
		for(var k=0; k<allRequests.length; k++) {
			for(var i=0; i<allRequests[k].length; i++) {
				if(allRequests[k][i].newpt.length > 0 && this.updateList.indexOf(allRequests[k][i]) != -1) {
					console.log("Updating request " + allRequests[k][i].number);
					try {
						allRequests[k][i].updateDesflow();	
					}
					catch(e) {
						throw {type: 'EasyPT', request: allRequests[k][i].number};
					}
				} 
			}
		}
	}	
	
};

EasyPTController.prototype.createAdhocRequest = function(requestNumber) {
	
	if(this.allRequestNumbers.indexOf(Number(requestNumber)) != -1) {
		return false;
	}
	
	var newAdhocRequest = new DesflowRequest(DEFAULT_BA, requestNumber, this.activeUser, true);
	this.adhocRequests.unshift(newAdhocRequest);
	this.allRequestNumbers.push(Number(requestNumber));
	this.UserStorage.addRequest(newAdhocRequest);
	return newAdhocRequest;
};

EasyPTController.prototype.removeAdhocRequest = function(adhocRequest) {
	var index = this.adhocRequests.indexOf(adhocRequest);
	if(index != -1) {
		this.UserStorage.removeRequest(adhocRequest);
		this.adhocRequests.splice(index, 1);		
		this.allRequestNumbers.splice(this.allRequestNumbers.indexOf(adhocRequest.number), 1);
	}
};

EasyPTController.prototype.removeAdhocRequestByNumber = function(request) {
	request = Number(request);
	var index = -1;
	for(var i=0; i<this.adhocRequests.length; i++) {
		if(this.adhocRequests[i].number == request) {
			index = i;
			break;
		}
	}
	
	if(index != -1) {
		this.UserStorage.removeRequest(this.adhocRequests[index]);
		this.adhocRequests.splice(index, 1);
		this.allRequestNumbers.splice(this.allRequestNumbers.indexOf(request), 1);
	}
};

EasyPTController.prototype.getUser = function() {
	return this.activeUser;
};

EasyPTController.prototype.getWeek = function() {
	return this.activeWeekRange;
};

EasyPTController.prototype.getWorkingDays = function() {
	return this.workingDays;
};

EasyPTController.prototype.getWeeklyRequests = function() {
	return this.weeklyRequests;
};

EasyPTController.prototype.getAdhocRequests = function() {
	return this.adhocRequests;
};

EasyPTController.prototype.getMgmtRequests = function() {
	return this.mgmtRequests;
};

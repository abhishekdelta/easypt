/**
 * @author Abhishek Shrivastava <shrivasa[at]deshaw.com>
 * @description EasyPT Frontend javascript
 */

var desflowRequestTemplate = Handlebars.compile($('#hb-desflow-request').html());
var alertMessageTemplate = Handlebars.compile($('#hb-alert-message').html());
var confirmstate = false;
var lockState = false;
var saveState = false;

$(document).ready(function() {
	
	$('#adhoc-request-add-btn').click(function() {
		var requestNumber = $('#adhoc-request-add-input').val();	
		if(!requestNumber || requestNumber == undefined) return;		
		
		$('#adhoc-request-add-input').val('');
		var desflowRequestObj = EasyPT.createAdhocRequest(requestNumber);
		
		console.log(desflowRequestObj);
		
		if(desflowRequestObj == false) {
			displayError(DEFAULT_BA + "#" + requestNumber + " already exists!");
			return false;
		}
		
		var status = desflowRequestObj.getData();
		if(status != true) {
			displayError(DEFAULT_BA + "#" + requestNumber + " either doesn't exists or you don't have enough permissions!");
			EasyPT.removeAdhocRequest(desflowRequestObj);
			return false;
		}
		
		var newDiv = $('<div/>').css('display','none');		
		newDiv.html(desflowRequestTemplate(desflowRequestObj));
		$('#adhoc-requests-container').prepend(newDiv);
		
		newDiv.fadeIn('slow');
		$('.desflow-request-pt').slimscroll({
	        color: '#666',
	        size: '8px',              
	        maxHeight: '120px'	                                                                                
		});
	});
	$('#adhoc-requests-container').on('click', '.adhoc-request-delete-btn', function() {	
		var div = $(this).closest('.desflow-request-div');
		request = div.attr('id').split("-")[2];
		EasyPT.removeAdhocRequestByNumber(request);
		div.fadeOut('slow', function(){
			div.remove();
		});		
	});
	
	$('#recalc-btn').click(function() {

		if(lockState) return;
		
		// Turn off save state
		saveState = false;
		
		var tts = 0.0;
		displayLoading('Recalculating...');
		
		// Disable lock button. Must save before.
		$('#lock-btn').attr('disabled', true);
		
		$('.desflow-request-div').each(function(index, request) {
			$(request).find('tr.new-pt-row input.new-pt-ts').each(function(index, input) {
				tts = tts + Number($(input).val());
			});
			$(request).find('div.new-pt-box input.new-pt-ts').each(function(index, input) {
				tts = tts + Number($(input).val());
			});
		});
		setTimeout(function() {
			var totaldays = EasyPT.getWorkingDays();
			
			$('#current-week-tts').html(String(Math.round(tts*100)/100));
			if(tts > totaldays) {
				$('#pt-progress-bar').removeClass('bar-success');
				$('#pt-progress-bar').addClass('bar-warning');
				$('#pt-progress-bar').css('width',"100%");
				$('#pt-remaining-bar').css('width',"0%");
			}
			else {
				$('#pt-progress-bar').removeClass('bar-warning');
				$('#pt-progress-bar').addClass('bar-success');
				$('#pt-progress-bar').css('width',(tts/totaldays)*100 + "%");
				$('#pt-remaining-bar').css('width',(1-tts/totaldays)*100 + "%");
			}			
			$('#pt-progress-bar').text(tts);
			$('#pt-remaining-bar').text(Math.round(100*(totaldays-tts))/100);
			clearAlerts();
		}, 500);
	});
	
	$('#save-btn').click(function() {
		
		if(lockState) return;
		
		displayLoading('Saving...');
		$('.desflow-request-div').each(function(index, request) {
			// desflow-risk-1223 -> 1223
			var requestId = $(request).attr('id').split("-")[2];
			var pt_list = [];
			$(request).find('tr.new-pt-row').each(function(index, row) {
				var ts = $(row).find('input.new-pt-ts').val();
				var pt = $(row).find('input.new-pt-detail').val();
				if(ts && pt) {
					pt_list.push([ts, pt]);
				}
			});
			
			$(request).find('div.new-pt-box').each(function(index, box) {
				var ts = $(box).find('input.new-pt-ts').val();
				var pt = $(box).find('textarea.new-pt-detail').val();
				if(ts && pt) {
					pt_list.push([ts, pt]);
				}			
			});
			EasyPT.saveRequest(requestId, pt_list);			
			
		});		
		setTimeout(function() {
			// Re-render page, just to sure
			displayLoading('Refreshing...');
			setTimeout(function() {
				// Refresh EasyPT and re-render page
				EasyPT.init(renderEasyPTUI);

				// Enable lock button
				$('#lock-btn').removeAttr('disabled');
				
				// Turn on save state
				saveState = true;
			}, 200);			
		}, 400);	
		
	});	
	
	// Using on because id is variable
	$('.control-panel').on('click', '#lock-btn', function() {		
		if(!saveState) return;
		
		displayLoading("Locking...");		
		$('input, textarea').attr('disabled',true);
		$('#recalc-btn').attr('disabled', true);
		$('#save-btn').attr('disabled', true);
		$('#adhoc-request-add-btn').attr('disabled', true);
		$('#push-btn').removeAttr('disabled');
		setTimeout(function() {
			$('#lock-btn').removeClass('btn-warning');
			$('#lock-btn').addClass('btn-inverse');
			$('#lock-btn > span').html('Unlock');
			$('#lock-btn').attr('id','unlock-btn');			
			lockState = true;
			clearAlerts();
			displaySuccess("Locked! Please note that <b>only last saved PT details</b> would be pushed to Desflow and any changes made after last save will be discarded.<br/>" +
						   "If you are unsure, please <b>Unlock</b>, <b>Save</b> and then <b>Lock</b> again.", 10000);
		}, 500);
	});
	
	$('.control-panel').on('click', '#unlock-btn', function() {		
		if(!lockState) return;
		
		displayLoading("Unlocking...");
		$('input, textarea').removeAttr('disabled');
		$('#recalc-btn').removeAttr('disabled');
		$('#save-btn').removeAttr('disabled');
		$('#adhoc-request-add-btn').removeAttr('disabled');
		$('#push-btn').attr('disabled', true);
		setTimeout(function() {
			$('#unlock-btn').removeClass('btn-inverse');
			$('#unlock-btn').addClass('btn-warning');
			$('#unlock-btn > span').text('Lock');
			$('#unlock-btn').attr('id','lock-btn');		
			lockState = false;		
			clearAlerts();
		}, 500);
	});
	
	$('#push-btn').click(function() {
		if(!lockState) return;
		
		confirmState = true;
				
		var list = EasyPT.getToBeUpdatedList();
		if(!list || list.length == 0) {
			displayError("There is nothing to update in Desflow! May be you didn't save your changes first or there aren't any!");
			return;
		}
		
		var html = "";
		for(var i=0; i<list.length; i++) {
			var newptts = 0.0;
			for(var j=0; j<list[i].newpt.length; j++) {
				newptts += Number(list[i].newpt[j].ts);
			}
			
			html += "<li><b>" + DEFAULT_BA + "#" + list[i].number + "</b> - " + list[i].title + " (PT: " + newptts + ")</li>";
		}	
		$('#desflow-push-confirm-list').html(html);
		clearAlerts();
		$('#desflow-push-confirmation').modal('show');				
	});
	
	$('#desflow-push-confirm-btn').click(function() {
		if(!confirmState) return;		
		displayLoading("Pushing to Desflow ...");
		
		var err = false;
		try {
			EasyPT.pushAllToDesflow();
		}
		catch(e) {
			err = true;
			displayError("Some problem occurred while pushing the request - " + DEFAULT_BA + "#" + e.request);
		}
		
		confirmState = false;
		
		setTimeout(function() {
			if(!err) {
				displaySuccess("Done! Your PT has been updated. Now relax and get back to work! :)");
				
				// Reload page
				setTimeout(function(){
					location.reload();
				}, 1000);
			}
		}, 500);
		
	});
	
	$('#desflow-push-cancel-btn').click(function(){
		if(!confirmState) return;
		confirmState = false;
		displaySuccess("Desflow Push Aborted!");
	});

});

function clearAlerts() {
	$('#alert-message .alert').remove();
}

function displayAlertMessage(message, type, isLoading) {
	// Clearing old alerts, if any
	clearAlerts();
	
	var context = {
		type : type,
		message : message,
		loading : isLoading		
	};
	
	var htmltemplate = alertMessageTemplate(context);
	
	//Set the popup window to center
	var winW = $(window).width();
	$('#alert-message').css('top', 45);
	$('#alert-message').html(htmltemplate);
	$('#alert-message').css('left', winW / 2 - ($('#alert-message').width() / 2));
}


function displayError(msg, delay) {
	displayAlertMessage(msg, "error", false);
	var delay = delay || 5000;
	$('#alert-message').fadeIn().delay(delay).fadeOut();
}

function displaySuccess(msg, delay) {
	displayAlertMessage(msg, "success", false);
	var delay = delay || 5000;
	$('#alert-message').fadeIn().delay(delay).fadeOut();
}

function displayLoading(msg) {
	if(msg == undefined)
		msg = " Loading... Please wait.";

	displayAlertMessage(msg, "info", true);
	$('#alert-message').fadeIn();
}


function renderEasyPTUI()
{
	user = EasyPT.getUser();
	week = EasyPT.getWeek();
	
	$('#user-name').html(user);
	
	$('#week-start-date').html(week[0].getFullYear() + ('0' + (week[0].getMonth()+1)).slice(-2) + ('0' + week[0].getDate()).slice(-2));
	$('#week-end-date').html(week[1].getFullYear() + ('0' + (week[1].getMonth()+1)).slice(-2) + ('0' + week[1].getDate()).slice(-2));

	requests = EasyPT.getWeeklyRequests();
	var container = $('#weekly-requests-container');
	var len = requests.length;
	
	if(len > 0) 
		container.empty();
	
	for(var i=0; i<len; i++) {		
		if(requests[i].getData()) {
			container.append(desflowRequestTemplate(requests[i]));
		}
	}
	
	requests = EasyPT.getAdhocRequests();
	var container = $('#adhoc-requests-container');
	var len = requests.length;
	
	if(len > 0)
		container.empty();
	
	for(var i=0; i<len; i++) {		
		if(requests[i].getData()) {
			container.append(desflowRequestTemplate(requests[i]));
		}
	}
	
	requests = EasyPT.getMgmtRequests();
	var container = $('#mgmt-requests-container');
	var len = requests.length;

	if(len > 0) 
		container.empty();
	
	for(var i=0; i<len; i++) {		
		if(requests[i].getData()) {
			container.append(desflowRequestTemplate(requests[i]));
		}
	}
	
	$('.desflow-request-pt').slimscroll({
        color: '#666',
        size: '8px',              
        maxHeight: '120px'	                                                                                
	});
	
	// Trigger recalculation. Give some time	
	$('#recalc-btn').click();
	
}

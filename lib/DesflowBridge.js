/**
 * @author Abhishek Shrivastava <shrivasa[at]deshaw.com>
 * @description DesflowBridge - A JS bridge to Desflow API for common read/write functions
 */

var _DesflowBridge = function(user, week) {
	this.user = user;
	this.week = week;
};

_DesflowBridge.prototype.updatePT = function(request, pt, details) {
	//@todo 
};

_DesflowBridge.prototype.readRequest = function(request) {
	//@todo
	return {status:'',title:'',ts:'',tts:'',pt:''};
};

_DesflowBridge.prototype.weekWorkingDays = function() {
	var aw = getActiveWeek();
	var fromdate = aw[0];
	var todate = aw[1];
	
	console.log(isValidDate(fromdate));
	console.log(isValidDate(todate));
	var fromStr = fromdate.getDate() + '/' + (fromdate.getMonth() + 1) + '/' + fromdate.getFullYear();
	var toStr = todate.getDate() + '/' + (todate.getMonth() + 1) + '/' + todate.getFullYear();
	console.log(fromStr);
	console.log(toStr);
	
	var users = ['ravipudm'];
	
	var LTS_SEARCH_URL_BASE = 'http://lts.hyd.deshaw.com/CalendarXML';
	var LTS_SEARCH_URL_STDPROPS = 'xmlFlag=true';

	var ltsurl = LTS_SEARCH_URL_BASE + '?' + 'fromDate=' + fromStr + '&' + 'toDate=' + toStr + '&' + 'employeeList=' + users.join() + '&' + LTS_SEARCH_URL_STDPROPS
	console.log(ltsurl);
	$.ajax({
		url 	 : ltsurl,
		dataType : 'jsonp text xml',
		type 	 : 'GET'
	});
	//$.get(ltsurl);
	// that fails!

	//@todo
	return 4.0;
}

_DesflowBridge.prototype.fetchWeeklyRequests = function() {
	// Fetches the user-assigned requests for this week with current status
	// 'Active' or any status change within this week.
	// @todo Add ability to get other week's data
	// @todo Also add option to pre-calculate the PT of this week by the user if any already in desflow
	return [ new DesflowRequest('Risk', 1000, this.user), 
	         new DesflowRequest('Risk', 2000, this.user) ];
};


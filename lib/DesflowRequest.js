/**
 * @author Abhishek Shrivastava <shrivasa[at]deshaw.com>
 * @description DesflowRequest - A JS class wrapper around a Desflow Request
 */

var DesflowRequest = function(ba, number, user, isAdhoc) {
	this.ba = ba;
	this.number = Number(number);
	this.user = user;
	this.isAdhoc = isAdhoc || false;
	
	// Fetch the request's local PT data (if any) during constructor call itself
	// as it is an asynchronous API and may take few millis so you can never be sure
	// when its done as the source call is sequential. The getData() method gets
	// desflow data for the same request, which will always be more time-consuming
	// than this and hence it can be expected that the lag in this asynchronous call
	// is always overshadowed by getData()
	// Get PT data not yet updated to Desflow
	this.newpt = false;
	var thisobj = this;
	EasyPT.loadRequest(number, function(pt_list) {		
		thisobj.newpt = new Array();
		if(pt_list) {
			for(var i=0; i<pt_list.length; i++) {
				if(pt_list[i][0] || pt_list[i][1]) {					
					thisobj.newpt.push({
						user : thisobj.user,
						ts   : pt_list[i][0],
						items: pt_list[i][1]
					});
				}
			}
			
			// Reversing the list so that the recent ones appear first
			thisobj.newpt.reverse();
		}
	}, function() {
		console.log("Unable to fetch local PT data for request:" + number);
	});
};

DesflowRequest.prototype.updateDesflow = function() {
	// Check if there's anything to update, it yes then do it
	if(this.newpt.length > 0) {
		try {
			for(var i=0; i<this.newpt.length; i++) {			
				EasyPT.desflowPush(this.number, this.newpt[i].ts, this.newpt[i].items);
			}
			EasyPT.clearRequestPT(this.number);
		} 
		catch(e) {
			
		}
	}	
};

DesflowRequest.prototype.getData = function() {
	// @todo
	// Gets the request details from desflow and updates the object's properties
	// Returns true if desflow query successful, else false, which might be
	// because of permission-issue or the request doesn't exist at all.
	
	var data = EasyPT.getRequestDataFromDesflow(this.number);
	if(data) {
		this.title = data.title;
		this.status = data.status;
		this.ts = data.ts;
		this.tts = data.tts;
		this.pt = data.pt;
	}
		
	// --- sample data begins ---
	if (this.number != 1000 && this.number != 515 && this.number != 2000)
		return false;
	
	// Queries Desflow to get all the data related to the request
	if (this.number == 1000 || this.number == 515) {
		this.title = 'Hello World';
		this.status = 'active';
		this.ts = 4.5;
		this.tts = 15;
		this.pt = [ {
			user : 'shrivasa',
			ts : 3,
			items : '*Did lots of Work.\n*More lots of Work'
		}, {
			user : 'saxenas',
			ts : 10.5,
			items : '*Bullshit!'
		}, {
			user : 'shrivasa',
			ts : 1.5,
			items : '*Did literally Nothing'
		} ];
	}
	if (this.number == 2000) {
		this.title = 'Hello World 2000 This is a request which is pending for so long.';
		this.status = 'closed';
		this.ts = 1.5;
		this.tts = 1.5;
		this.pt = [ {
			user : 'shrivasa',
			ts : 1.5,
			items : '*Did lots of Work.\n*More lots of Work and lots and lots.'
		}, {
			user : 'saxenas',
			ts : 0,
			items : '*121Bullshit!'
		} ];
	}
	// --- sample data ends ---
	
	// Required logic
	if(this.status == 'closed')
		this.isClosedStatus = true;
	if(this.status == 'open' || this.status == 'active' )
		this.isActiveStatus = true;
	if(this.status == 'suspended')
		this.isSuspendedStatus = true;
	
	var ptLength = this.pt.length;
	for(var i=0; i<ptLength; i++) {
		this.pt[i].items = nl2br(this.pt[i].items);	
	}

	return true;
};

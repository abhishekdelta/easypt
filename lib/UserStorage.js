/**
 * @author Abhishek Shrivastava <shrivasa[at]deshaw.com>
 * @description UserStorage - For storing the user's local data in browser
 */

var _UserStorage = function(user, week) {
	this.user  = user;
	this.week  = week;
	
	var weekstr = week[0].getFullYear() + ('0' + (week[0].getMonth()+1)).slice(-2) + ('0' + week[0].getDate()).slice(-2);
	weekstr += '-' + week[1].getFullYear() + ('0' + (week[1].getMonth()+1)).slice(-2) + ('0' + week[1].getDate()).slice(-2);
	
	this.key   = DEFAULT_BA + '_' + user + '_' + weekstr;
	this.store = new Persist.Store(this.key); 
	
	// Sanity check
	this.store.remove('test_key');
	this.store.set('test_key', this.key);
	this.store.get('test_key', function(ok, val) {
		if(!ok) {
			displayError("Browser storage could not be initialized! Your data would be lost if you close EasyPT.");
		}
	});
	
	console.log("Using storage:" + Persist.type + " with key:" + this.key);
};

_UserStorage.prototype.save = function (key, value) {
	this.store.set(key, value);
};

_UserStorage.prototype.load = function (key, callback) {
	this.store.get(key, callback);
};

_UserStorage.prototype.clear = function(key) {
	this.store.remove(key);
};

_UserStorage.prototype.saveRequestPT = function(request, pt_list) {
	var backup;
	request = String(request);
	var store = this.store;
	this.store.get(request, function(ok, val) {		
		// If some data already exists, create a backup.
		if(ok) {
			backup = val;			
		}
		var json_data = JSON.stringify(pt_list);
		try {						
			// Save the request PT in JSON format
			store.set(request, json_data);		
		}
		catch(e) {
			if(backup != undefined) {
				// Anything goes wrong, restore from backup if it exists
				store.set(request, backup);
				console.log("Not able to save for:" + request);
			}
		}				
	});	
};

_UserStorage.prototype.loadRequestPT = function(request, callback, error) {	
	this.store.get(String(request), function(ok, val) {
		if(ok) {			
			callback(JSON.parse(val));						
		}
		else if(error) {
			error();
		}
	});	
};

_UserStorage.prototype.fetchAdhocRequests = function(callback, error) {
	this.store.get(ADHOC_REQUEST_KEY, function(ok, val) {
		if(ok) {
			callback(JSON.parse(val));
		}
		else if(error) {
			error();
		}
	});
};

_UserStorage.prototype.addRequest = function(requestObject) {
	
	var request = requestObject.number;
	var store = this.store;
	this.store.get(ADHOC_REQUEST_KEY, function(ok, val) {
		if(ok) {
			var list = JSON.parse(val) || new Array();				
			list.unshift(request);
			store.set(ADHOC_REQUEST_KEY, JSON.stringify(list));
		}
	});
};

_UserStorage.prototype.removeRequest = function(requestObject) {
	var request = requestObject.number;
	var store = this.store;
	this.store.get(ADHOC_REQUEST_KEY, function(ok, val) {
		if(ok) {
			var list = JSON.parse(val) || new Array();				
			var index = list.indexOf(request);
			if(index != -1) {
				list.splice(index, 1);
			}
			store.set(ADHOC_REQUEST_KEY, JSON.stringify(list));
		}
	});
	
	// Also remove any associated user-added PT data
	this.store.remove(request);
};


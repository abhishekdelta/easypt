/**
 * @author Abhishek Shrivastava <shrivasa[at]deshaw.com>
 * @description EasyPT Configuration
 */

var DEFAULT_BA = 'Risk';

var MANAGEMENT_REQUESTS = [ 515, // Request to track meetings
		1000, // Request to track management tasks
		2000 // Request to track trainings
];

var ADHOC_REQUEST_KEY = 'adhoc';
/**
 * @author Abhishek Shrivastava <shrivasa[at]deshaw.com>
 * @description Utility functions
 */

function getActiveUser() {
	// @todo Gets the user name of the current active user who's using this tool
	return 'shrivasa';
}

function getActiveWeek() {
	// Gets the current active week
	var curr = new Date; // get current date
	var first = curr.getDate() - curr.getDay() + 1; // First day is the day of the month - the day of the week
	var last = first + 6; // last day is the first day + 6
	var firstday = new Date(curr.setDate(first));
	var lastday = new Date(curr.setDate(last));
	return [firstday, lastday];
}

function nl2br(str) {
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br/>' + '$2');
}

/*
 * for a given Date and int offset,
 * return the week range covering the date, slid by given offset in weeks 
 */
function getWeek(aDate, offset) {
	// aDate is a Date object and offset is int
	// get week covering given date, and if offset is given, move the range accordingly
	var first = aDate.getDate() - aDate.getDay() + 1 + offset * 7;
	var last = first + 6; 
	var firstday = new Date(aDate.setDate(first));
	var lastday = new Date(aDate.setDate(last));
	return [firstday, lastday]
}

function isValidDate(d) {
	if ( Object.prototype.toString.call(d) !== "[object Date]" )
		return false;
	return !isNaN(d.getTime());
}